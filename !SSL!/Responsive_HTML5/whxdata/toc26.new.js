(function() {
var toc =  [{"type":"item","name":"Using the Dimensions Browser","url":"Using_the_Dimensions_Browser.htm"},{"type":"item","name":"Configuring Page and Report Properties for Charts","url":"Configuring_Page_Properties.htm"},{"type":"item","name":"Changing the Chart Type","url":"Changing_Chart_Types.htm"},{"type":"item","name":"Deleting a Chart","url":"Deleting_a_chart.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();